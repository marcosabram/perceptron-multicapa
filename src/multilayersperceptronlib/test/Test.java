package multilayersperceptronlib.test;
/*
 *     mlp-java, Copyright (C) 2012 Davide Gessa
 *
 * 	This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

import multilayersperceptronlib.MultiLayerPerceptron;
import multilayersperceptronlib.transferfunctions.HeavysideTransfer;
import multilayersperceptronlib.transferfunctions.HyperbolicTransfer;
import multilayersperceptronlib.transferfunctions.SigmoidalTransfer;

public class Test
{
    /**
     * @param args
     */
    public static void main(String[] args) throws IOException {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        ArrayList<double[]> inputs = new ArrayList<double[]>();
        ArrayList<double[]> outputs = new ArrayList<double[]>();

        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File("/home/marcos/Documentos/Sistemas Inteligentes/PerceptronMulticapa/src/multilayersperceptronlib/datasetdiabetes.txt");
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            // Lectura del fichero
            String linea;
            while ((linea = br.readLine()) != null) {
                //System.out.println(linea);
                double[] inputAux = new double[8];
                double[] outputAux = new double[1];
                double max = 0.0;
                String[] valores = linea.split(",");
                for (int i = 0; i < 7; i++) {
                    inputAux[i] = Double.parseDouble(valores[i]);
                    if (inputAux[i] > max) {
                        max = inputAux[i];
                    }
                    //System.out.println(patron[i]);
                }
                for (int i = 0; i < 7; i++) {
                    inputAux[i] = (inputAux[i] / max); //normalizacion
                }
                inputs.add(inputAux);
                if (Double.parseDouble(valores[8]) == 0) {
                    outputAux[0] = -0.5;
                    outputs.add(outputAux);//normalizacion
                } else if (Double.parseDouble(valores[8]) == 1) {
                    outputAux[0] = 0.5;
                    outputs.add(outputAux);//normalizacion
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto sitodo va bien como si salta
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        /*para aplicar la aleatoridad en la eleccion de los patrones, se cargar por una unica vez el archivo en memoria principal
        luego de aqui en mas, no se consulta mas a memoria secundaria, dado que es costoso, simplemente se crean estructuras logicas para
        representar los indices en la estructura de dato inputs y output, que son las que en verdad tienen almacenados los patrones
         */
        Vector patrones;
        patrones = new Vector(inputs.size()); //mantiene todos los indices que referencian a input y outpur

        int learn = (int) (inputs.size() * (0.7));//numeros de patrones para el aprendizaje

        Vector learning;
        learning = new Vector(learn);// almacena los indices que corresponden a inputs y output (patrones) que se utilizaran en el aprendizaje

        for (int i = 0; i < inputs.size(); i++) {
            patrones.add(i); /*se cargan los indices de inputs y ouputs en patrones, en un principio el indice i coincidira
            con el valor que almacena patrones en i, a su vez i referencia tanto al patron en inputs como a su correspondiente salida en outputs*/
        }

        Random rd = new Random();

        for (int i = 0; i < learn; i++) {
            int index = rd.nextInt(patrones.size());
            learning.add(patrones.get(index)); //cargos indices aleatorias en learnening, asi es como implementamos la aleatoridad
            patrones.remove(index);
        }
        //los que queden en patrones se utilizaran para generalizar

        int[] layers = new int[]{8, 25, 1};
        //5 0.0005 0.001 memoriza
        // 10 0.01 0.001 memoriza
        // 5, eta=0.005,  alfa=0.001 ECM aprendizaje baja hasta 18, el ECM test se clava en 20
        // 15, eta=0.005,  alfa=0.001 ECM aprendizaje baja hasta 15, el ECM de test sube hasta 24
        //10 eta 0005 alfa 0.01 ECM aprendizaje baja hata 18, el ECM de test sube hasta 20
        MultiLayerPerceptron net = new MultiLayerPerceptron(layers, 0.0001, new HyperbolicTransfer());
        MultiLayerPerceptron netCopy = net;

        Vector cargados;
        cargados = new Vector(learn);
        //Utilidades
        int indexRandom;
        int indexL, indexT;
        double sumaError = 0.0;
        //Error cuadratico medio en aprendizaje
        double ECM = 0.0;
        Vector errores = new Vector();
        double minECM = 1.0;
        //Error cuadratico medio en testing
        double ECMT = 0.0;
        double minECMT = 1.0;
        //brecha de epocas en las cuales realizo test durante el aprendizaje
        int epocaEtaAdaptativo = 5;

        /* Learning */
        BufferedWriter outputFileE=new BufferedWriter(new FileWriter("src//ECM.txt",true));
        BufferedWriter outputFileE1=new BufferedWriter(new FileWriter("src//ECMT.txt",true));
        BufferedWriter outputFileE2=new BufferedWriter(new FileWriter("src//LearningRate.txt",true));
        String resultado;

        for (int j = 0; j < 200000; j++) {
            cargados = new Vector(learn);
            for (int i = 0; i < learn; i++) {
                cargados.add(i);
            }
            Random rdNew = new Random();
            for (int i = 0; i < learn; i++) {
                // for (int i = 0; i < learning.size(); i++) {

                indexRandom = rdNew.nextInt(cargados.size());
                indexL = (int) cargados.get(indexRandom);
                double[] input = inputs.get((int) learning.get(indexL));
                double[] output = outputs.get((int) learning.get(indexL));

                cargados.remove(indexRandom);
                double error;

                error = net.backPropagate(input, output);

                sumaError += Math.pow(error, 2);
            }
            ECM = sumaError / learn;
            errores.add(ECM);
            sumaError = 0.0;
            if (ECM < minECM) {
                minECM = ECM;
            }
            //System.out.println(ECM);
            if (epocaEtaAdaptativo == j) {

                for (int i = 0; i < patrones.size(); i++) {
                    indexT = (int) patrones.get(i);
                    double[] input = inputs.get(indexT);
                    double[] output = net.execute(input);
                    sumaError += Math.pow((output[0] - outputs.get(indexT)[0]), 2);
                }
                ECMT = sumaError / patrones.size();
                sumaError = 0.0;
                if (ECMT < minECMT) {
                    minECMT = ECMT;
                }
                resultado=j + "," + ECM +"\n";
                outputFileE.write(resultado);
                resultado=j + "," + ECMT +"\n";
                outputFileE1.write(resultado);
                resultado=j + "," + net.getLearningRate()+"\n";
                outputFileE2.write(resultado);
                System.out.println("Epoca " + j + "   ECM de aprendizaje: " + ECM + "      ECM de testeo: " + ECMT);

                //analizo si se cumple el decremento constante, si con igual o si aumentaron
               boolean iguales = true;
                for (int i = 1; i < errores.size() - 1; i++) {
                    if ((double) errores.get(i - 1) != (double) errores.get(i)) {
                        iguales = false;
                        i = errores.size();
                    }
                }

                if (iguales == false) {
                    boolean menor = false;
                    for (int i = 0; i < errores.size() - 2; i++) {
                        if ((double) errores.get(i + 1) < (double) errores.get(i)) {
                            menor = true;
                        } else {
                            menor = false;
                            i = errores.size();
                        }
                    }
                    if (menor == true) {
                        //copiamos la RNA para el proximo analisis
                        netCopy = net;
                        //aplicamos la adaptacion
                        net.setLearningRate(net.getLearningRate() + 0.001);
                    } else {
                        //a la RNA le asignamos la RNA anterior
                        net = netCopy;
                        errores.clear();
                        //aplicamos la adaptacion
                        net.setLearningRate(net.getLearningRate() * (1-0.025));
                    }
                } else {
                    netCopy = net;
                    errores.clear();
                }
                epocaEtaAdaptativo += 5;
            }
        }
        outputFileE.close();
            System.out.println("Learning completed!");
            System.out.println("Minimo ECM: " + minECM);
            System.out.println("Minimo ECM: " + minECMT);
            /* Test */
            System.out.println("Test!");
            for (int i = 0; i < patrones.size(); i++) {
                int index = (int) patrones.get(i);
                double[] input = inputs.get(index);
                double[] output = net.execute(input);
                sumaError += Math.pow((output[0] - outputs.get(index)[0]), 2);
            }
            ECMT = sumaError / patrones.size();
            System.out.println(ECMT);
        }


    }






